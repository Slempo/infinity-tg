/obj/item/clothing/shoes/antiquated_shoes
	name = "antiquated shoes"
	desc = "Really old, out of fashion shoes"
	icon_state = "antiquated_shoes"
	item_state = "antiquated_shoes"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/shoes/brand1
	name = "white voltage shoes"
	desc = "Stylish white shoes.Anyooh production."
	icon_state = "brand_shoes1"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/shoes/brand2
	name = "black redline shoes"
	desc = "Stylish black shoes. Anyooh production."
	icon_state = "brand_shoes2"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/shoes/shoes_rim
	name = "shoes with a rim"
	desc = "Stylish black shoes. Anyooh production."
	icon_state = "shoes_rim"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/shoes/black_open_shoes
	name = "open shoes"
	desc = "Stylish black shoes. Anyooh production."
	icon_state = "black_open_shoes"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'