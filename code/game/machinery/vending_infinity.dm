/obj/machinery/vending/clothing/outerspace
	name = "\improper Outerspace-Clothes"
	desc = "Outerspace designer brand machine! For truly style!"
	icon = 'icons/obj/infinity_object.dmi'
	icon_state = "outerspace_clothe"
	product_slogans = "Stop right here! Outerspace designer here! ;It's time for new clothe-experience! ;What? Never see that before?"
	vend_reply = "Outerspace designer thanks you!"
	products = list(/obj/item/clothing/under/mai_yang=2,/obj/item/clothing/under/dress_pink=2,/obj/item/clothing/under/dress_green=2,
		/obj/item/clothing/under/sundress_white=3,/obj/item/clothing/under/dress_fire=3,/obj/item/clothing/under/dress_yellow=2,/obj/item/clothing/under/dress_saloon=1,
		/obj/item/clothing/under/schoolgirlblack=1,/obj/item/clothing/suit/brand/blue_jacket=2,/obj/item/clothing/suit/hooded/black_hoody=2,
		/obj/item/clothing/suit/toggle/jacket=2,/obj/item/clothing/suit/dress_orange=1,/obj/item/clothing/suit/bride_white=1,
		/obj/item/clothing/suit/brand/orange_jacket=2,/obj/item/clothing/under/red_cheongasm=2,/obj/item/clothing/shoes/ballets/red_gold=2,
		/obj/item/clothing/suit/toggle/fiery_jacket=2,/obj/item/clothing/suit/toggle/white_fiery_jacket=2,/obj/item/clothing/suit/leon_jacket=1,/obj/item/clothing/under/latex_pants=1,
		/obj/item/clothing/head/bowknot=1,/obj/item/clothing/under/kos_shorts=2,/obj/item/clothing/under/kos_bshorts=2,/obj/item/clothing/under/kos_bshorts2=2,/obj/item/clothing/under/womentshirt=1,
		/obj/item/clothing/suit/jacket/leather/overcoat=2, /obj/item/clothing/suit/longjacket=2, /obj/item/clothing/under/shorts_denim=2, /obj/item/clothing/suit/brand/reg_jacket=2,
		/obj/item/clothing/suit/brand/brand_rjacket=1, /obj/item/clothing/suit/latex_top=2, /obj/item/clothing/suit/handless_latex_top=2, /obj/item/clothing/under/greyskirt=3, /obj/item/clothing/under/pinkskirt=3,
		/obj/item/clothing/under/rank/vice2=2, /obj/item/clothing/under/doubleskirt=2, /obj/item/clothing/head/captoe=2, /obj/item/clothing/head/captoe/black=2, /obj/item/clothing/under/redsuit=3,
		/obj/item/clothing/shoes/brand1=3, /obj/item/clothing/shoes/brand2=5, /obj/item/clothing/under/black_skirt=3, /obj/item/clothing/shoes/black_open_shoes=2, /obj/item/clothing/shoes/shoes_rim=5,
		/obj/item/clothing/under/jeanschain=3, /obj/item/clothing/under/black_dress=2)
	premium = list(/obj/item/clothing/under/snow_maiden=1, /obj/item/clothing/under/snow_maiden/blue=1, /obj/item/clothing/under/candy_dress=2, /obj/item/clothing/under/bunny_suit=2,
		/obj/item/clothing/suit/kimono/black=1, /obj/item/clothing/suit/kimono=1, /obj/item/clothing/suit/kimono/blue=1, /obj/item/clothing/head/lify_hat=1, /obj/item/clothing/under/lify=1)
